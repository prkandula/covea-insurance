﻿using System;
using InsuranceCalculator;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InsuranceValidator.Test
{
    [TestClass]
    public class InsuranceValidatorTests
    {
        [TestMethod]
        public void Given_A_NegativeInteger_Should_Return_False()
        {
            PremiumCalculator sut = new PremiumCalculator();
            bool result = sut.IsDataValid(-23, 340000);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Given_An_zero_Int_Should_Return_False()
        {
            PremiumCalculator sut = new PremiumCalculator();
            bool result = sut.IsDataValid(0,0);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Given_An_Longer_Than_Int_Should_Return_False()
        {
            PremiumCalculator sut = new PremiumCalculator();
            bool result = sut.IsDataValid(66, 500001);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Given_A_int_which_is_min_Value_Should_Return_False()
        {
            PremiumCalculator sut = new PremiumCalculator();
            bool result = sut.IsDataValid(59,250090);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void GetPremium_Value_Should_Return_False()
        {
            PremiumCalculator sut = new PremiumCalculator();
            string result = sut.GetPremium(30,25000);
            Assert.AreEqual(result, 2.108);
        }
    }
}
