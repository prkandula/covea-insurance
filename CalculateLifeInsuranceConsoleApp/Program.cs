﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculateLifeInsuranceConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            // Declare variables and then initialize to zero
            int Age = 0; int SumAssured = 0;

            // Display title as the Calculate LifeInsurance --Console App
            Console.WriteLine("Calculate Life Insurance\r");
            Console.WriteLine("------------------------\n");

            // Ask the user to type the Age
            Console.WriteLine("Enter Age, and then press Enter");
            Age = Convert.ToInt32(Console.ReadLine());

            // Ask the user to type the Sum Assured
            Console.WriteLine("Enter Sum Assured, and then press Enter");
            SumAssured = Convert.ToInt32(Console.ReadLine());

            InsuranceCalculator.PremiumCalculator insurance = new InsuranceCalculator.PremiumCalculator();

           string Premium = insurance.GetPremium(Age,SumAssured);
           
            Console.WriteLine($"Quoted Age: {Age} and Sum Assured :£ {SumAssured}");
            Console.WriteLine($"The Premium avaliable for the Age: {Age} and Sum Assured: £" + Premium.ToString());
            // Wait for the user to respond before closing
            Console.Write("Press any key to close the Calculate Life Insurance...");
            Console.ReadKey();
        }
    }
}
