﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsuranceCalculator
{
    public class PremiumCalculator
    {
        List<RateBand> rateBands = new List<RateBand>();
        public PremiumCalculator ()
        {
            rateBands.Add(new RateBand() { MinAge = 18, MaxAge = 30, SumAssured = 25000, Rate = 0.0172 });
            rateBands.Add(new RateBand() { MinAge = 18, MaxAge = 30, SumAssured = 50000, Rate = 0.0165 });
            rateBands.Add(new RateBand() { MinAge = 18, MaxAge = 30, SumAssured = 100000, Rate = 0.0154 });
            rateBands.Add(new RateBand() { MinAge = 18, MaxAge = 30, SumAssured = 200000, Rate = 0.0147 });
            rateBands.Add(new RateBand() { MinAge = 18, MaxAge = 30, SumAssured = 300000, Rate = 0.0144 });
            rateBands.Add(new RateBand() { MinAge = 18, MaxAge = 30, SumAssured = 500000, Rate = 0.0146 });

            rateBands.Add(new RateBand() { MinAge = 31, MaxAge = 50, SumAssured = 25000, Rate = 0.1043 });
            rateBands.Add(new RateBand() { MinAge = 31, MaxAge = 50, SumAssured = 50000, Rate = 0.0999 });
            rateBands.Add(new RateBand() { MinAge = 31, MaxAge = 50, SumAssured = 100000, Rate = 0.0932 });
            rateBands.Add(new RateBand() { MinAge = 31, MaxAge = 50, SumAssured = 200000, Rate = 0.0887 });
            rateBands.Add(new RateBand() { MinAge = 31, MaxAge = 50, SumAssured = 300000, Rate = 0.0872 });
            rateBands.Add(new RateBand() { MinAge = 31, MaxAge = 50, SumAssured = 500000, Rate = null });


            rateBands.Add(new RateBand() { MinAge = 51, MaxAge = 65, SumAssured = 25000, Rate = 0.1043 });
            rateBands.Add(new RateBand() { MinAge = 51, MaxAge = 65, SumAssured = 50000, Rate = 0.2565 });
            rateBands.Add(new RateBand() { MinAge = 51, MaxAge = 65, SumAssured = 100000, Rate = 0.2393 });
            rateBands.Add(new RateBand() { MinAge = 51, MaxAge = 65, SumAssured = 200000, Rate = 0.2285 });
            rateBands.Add(new RateBand() { MinAge = 51, MaxAge = 65, SumAssured = 300000, Rate = null });
            rateBands.Add(new RateBand() { MinAge = 51, MaxAge = 65, SumAssured = 500000, Rate = null });
        }
            
        public string GetPremium(int age, int sumAssured)
        {
            double ?rate; string na = "Not available"; double premium; string result = string.Empty;
            if (IsDataValid(age, sumAssured) == true)
            {
                rate = GetRate(age, sumAssured);
                if (rate.HasValue)
                {
                    premium = GetGrossPremium(rate.Value, sumAssured);
                    result = sumAssured + " is: £" + premium;
                    while (premium < 2)
                    {
                        sumAssured = sumAssured + 5000;
                        rate = GetRate(age, sumAssured);
                        if (rate.HasValue)
                        {
                            premium = GetGrossPremium(rate.Value, sumAssured) ;
                            result = sumAssured + " is: £" + premium;
                        }
                        else
                        { 
                            return na;
                        }
                    }
                    return result;
                }               
            }
             return na; 
            
        }

        

        public bool IsDataValid(int age, int sumAssured)
        {
                                              
           return (age >= 18 && age <= 65) & (sumAssured >= 25000 && sumAssured <= 500000);
                        
        }

        private double?  GetRate(int age, int sumAssured)
        {
            var selectedRate= rateBands.Where(t => t.MinAge <= age && t.MaxAge >= age && t.SumAssured == sumAssured).FirstOrDefault();
            if (selectedRate != null)
            {
                return selectedRate.Rate; 
            }
            else
            {
                return CalculateRate(age, sumAssured);
            }
                                     
        }

        private double? CalculateRate(int age, int sumAssured)
        {
            var lowerBand = rateBands.Where(t => t.SumAssured < sumAssured).OrderByDescending(t => t.SumAssured).FirstOrDefault();
            var upperBand = rateBands.Where(t => t.SumAssured > sumAssured).OrderBy(t => t.SumAssured).FirstOrDefault();
            double v1 = (double)(sumAssured - lowerBand.SumAssured) / (double)(upperBand.SumAssured - lowerBand.SumAssured);
            double lBand = (double)(v1 * upperBand.Rate.Value);
            double v2 = (double)(upperBand.SumAssured - sumAssured) / (double)(upperBand.SumAssured - lowerBand.SumAssured);
            double uBand = (double)(v2 * lowerBand.Rate.Value);
            double? riskRate = lBand + uBand;                        return riskRate;
        }

        private double GetGrossPremium(double rate, int sumAssured)
        {
            double grossPremium = 0;
            double riskPremium = rate * (sumAssured / 1000);
            double renewalCommission = 0.03 * riskPremium;
            double netPremium = riskPremium + renewalCommission;
            double initialCommission = netPremium * 2.05;
            grossPremium = netPremium + initialCommission;
                  
            return grossPremium;
        }
        

    }
}
