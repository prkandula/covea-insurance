﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsuranceCalculator
{
    public class RateBand
    {
        public int MinAge { get; set; }
        public int MaxAge { get; set; }
        public int SumAssured { get; set; }
        public Double? Rate {get;set;}
    }
}
